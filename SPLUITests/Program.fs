﻿open canopy
open canopy.core
open canopy.types
open runner
open configuration
open reporters

open common
open database

open System
open System.IO
open System.Reflection
open System.Drawing
open OpenQA.Selenium

let loginWith browser =
  switchTo browser
  browser.Url <- url
  "#firstname" << "Doris"
  "#lastname" << "Adams"
  "#email" << customerEmail
  "#zipcode" << "93277"
  click ".lp-button"

let submitAccountInfoWith browser =
  switchTo browser
  "#CreditReportForm_tbAddressSt_tb" << "2635 W. Evegreen AV"
  "#CreditReportForm_tbPassword_tb" << "password1"
  "#CreditReportForm_tbPasswordVerify_tb" << "password1"
  "#CreditReportForm_tbSecretAnswer_tb" << "test"
  click "#CreditReportForm_btnSubmitPage1"

let submitEzApplySSNWith browser last4 =
  switchTo browser
  "#CreditReportForm_tbEzApplySSN_tbSSN3ez" << last4
  "#CreditReportForm_tbDOB_Month" << "May"
  "#CreditReportForm_tbDOB_Day" << "30"
  "#CreditReportForm_tbDOB_YearDropDown" << "1980"
  click "#CreditReportForm_btnSubmitPageN"

let submitFullSsnWith browser =
  switchTo browser
  "#CreditReportForm_tbEzApplySSN_tbSSN1" << customerSsn1
  "#CreditReportForm_tbEzApplySSN_tbSSN2" << customerSsn2
  "#CreditReportForm_tbEzApplySSN_tbSSN3" << customerSsn3
  click "#CreditReportForm_btnSubmitPageN"

let answerQuestionsWith browser =
  switchTo browser
  if exists "Which of these cities are you associated with?" then 
    if exists (radioOptionFormat "Chino") then click (radioOptionFormat "Chino")
    if exists (radioOptionFormat "Oakdale") then click (radioOptionFormat "Oakdale")
  if exists "In 2007, what county did you live in?" then click (radioOptionFormat "Stanislaus")
  if exists "In 2001 you applied for a hunting or fishing license in which of the following states?" then click (radioOptionFormat "Florida")
  if exists "Which of the following hunting or fishing licenses do you have or have you had in the past?" then click (radioOptionFormat "Duck")
  if exists "In what county do you live?" then click (radioOptionFormat "Los Angeles")
  if exists "From the following list, select the city in which you lived in 1999" then click (radioOptionFormat "Chino")
  if exists "In 1999, what county did you live in?" then click (radioOptionFormat "San Bernardino")
  if exists "From the following list, select the city in which you lived in 2007" then click (radioOptionFormat "Oakdale")
  if exists "In which of the following states have you had a driver's license?" then click (radioOptionFormat "Florida")
  if exists "What year did you graduate from high school?" then click (radioOptionFormat "1978")
  if exists "Which of the following addresses belongs to a business you are currently or previously associated with?" then click (radioOptionFormat "44 River")
  if exists "From the following list, select one of your current or previous employers." then click (radioOptionFormat "Pepsi Co")
  if exists "Which of these businesses are you associated with?" then click (radioOptionFormat "Pepsi Co.")
  click "#CreditReportForm_btnSubmitPageQ"

let fillInCreditInformationWith browser =
  switchTo browser
  "#CreditReportForm_tbCreditCardUserName_tb" << "Doris Adams"
  "#CreditReportForm_tbCreditCardNumber_tb" << "4444444444444448"
  "#CreditReportForm_ddlCreditCardExpirationDate_ddlMonth" << "08"
  "#CreditReportForm_ddlCreditCardExpirationDate_ddlYear" << "2023"
  "#CreditReportForm_tbCreditCardSecurityCode_tb" << "444"
  click "#CreditReportForm_btnSubmitPage2"

let successfull4DigitEZApply browser =
  loginWith browser
  on addressPageUrl
  submitAccountInfoWith browser
  on ezApplyUrl
  submitEzApplySSNWith browser customerSsn3
  on questionsUrl
  answerQuestionsWith browser
  on creditCardUrl
  fillInCreditInformationWith browser
  on orderCompleteUrl

[<EntryPoint>]
let main argv =

  context "should enroll with EZ Apply"

  let b = startBrowser()

  once(fun _ ->
    seedData customerHashedSsn |> ignore
  )
  
  before(fun _ -> 
    deActivateCustomer customerEmail
    b.Manage().Cookies.DeleteAllCookies()
  )
  
  test(fun _ ->
    describe "running through ez apply workflow"
    successfull4DigitEZApply b
  )

  test(fun _ ->
    describe "fail EZApply, Enroll with 9 digit"
    loginWith browser
    on addressPageUrl
    submitAccountInfoWith browser
    on ezApplyUrl
    submitEzApplySSNWith browser "5678"
    on ezApplyUrl
    submitFullSsnWith browser
    on questionsUrl
    answerQuestionsWith browser
    on creditCardUrl
    fillInCreditInformationWith browser
    on orderCompleteUrl
  )

  lastly(fun _ ->
    deleteSeedData customerHashedSsn
  )

  runner.run()

  printfn "press [enter] to exit"
  System.Console.ReadLine() |> ignore

  quit b

  runner.failedCount