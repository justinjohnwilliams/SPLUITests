﻿module common

open canopy
open canopy.core
open canopy.types
open runner
open configuration
open reporters

open System
open System.IO
open System.Reflection
open System.Drawing
open OpenQA.Selenium

let customerEmail = "Doris@ot.net"
let customerHashedSsn = "iYR+rrjUTq3+xy0Ok/t4VcFkYLiN1ze6rDDNLBcpkO4="
let customerSsn1 = "300"
let customerSsn2 = "00"
let customerSsn3 = "5001"

let addressPageUrl = "http://spendonlife.local/us/6019/373f031/t420/196/f/cs_4p_a196.aspx?address=&city=&state=&firstname=Doris&lastname=Adams&email=Doris%40ot.net&zipcode=93277&cbxMailOffersAndDiscounts=on"
let ezApplyUrl = "http://spendonlife.local/us/6019/373f031/t420/196/f/cs_4p_n196.aspx?pre=2241&c=MCAwIDAgMCAwIDMyNjIgMDEwMTAwMDEwMDAwMDA%3d"
let questionsUrl = "http://spendonlife.local/us/6019/373f031/t420/196/f/cs_4p_q196.aspx?pre=2241"
let creditCardUrl = "http://spendonlife.local/us/6019/373f031/t420/196/f/cs_4p_b196.aspx?pre=2241"
let orderCompleteUrl = "http://spendonlife.local/us/6019/373f031/t420/196/f/cs_4p_s196.aspx?pre=2241&memberid=2149"

[<Literal>]
let env = "local"

let url = 
  match env with
  | "local" -> "http://spendonlife.local/us/6019/373f031/t420/196/lp/196-a87c/index.aspx"
  | "corpdev12" -> "http://spl.corpdev12.onetech.local/us/6019/373f031/t420/196/lp/196-a87c/"
  | "corpdev14" -> "http://spl.corpdev14.onetech.local/us/6019/373f031/t420/196/lp/196-a87c/"
  | _ -> "http://spendonlife.local/us/6019/373f031/t420/196/lp/196-a87c/index.aspx"

let connectionString =
  match env with
  | "local" -> "Data Source=.;Initial Catalog=CreditFulfillment;Integrated Security=SSPI;"
  | "corpdev12" -> "Data Source=(corpdev12);Initial Catalog=CreditFulfillment;Integrated Security=SSPI;"
  | "corpdev14" -> "Data Source=(corpdev14);Initial Catalog=CreditFulfillment;Integrated Security=SSPI;"
  | _ -> ""

let alwaysTest f = null &&&&& f

let exists selector =
  let e = someElement selector
  match e with
    | Some(e) -> true
    | _ -> false

let openBrowser _ =
  configuration.chromeDir <- Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
  let options = Chrome.ChromeOptions()
  options.AddArgument("--enable-logging")
  options.AddArgument("--v=0")
  start (ChromeWithOptions options)
  browser.Manage().Cookies.DeleteAllCookies()

let startBrowser _ =
  openBrowser()
  browser

let radioOptionFormat s = sprintf "%s\n\t\t\t\t\t" s