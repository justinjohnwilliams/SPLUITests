﻿module database

open System;
open System.Data;
open System.Data.SqlClient;

open FSharp.Data.Sql
open FSharp.Data.Sql.Common

let [<Literal>] connectionString = "Data Source=(local);Initial Catalog=CreditFulfillment;Integrated Security=SSPI;"

type sql = SqlDataProvider<DatabaseProviderTypes.MSSQLSERVER,
                           connectionString,
                           CaseSensitivityChange = CaseSensitivityChange.ORIGINAL>

let ctx = sql.GetDataContext()

let getActiveAccountsFor email =
  query {
    for customer in ctx.Dbo.Customers do
        join account in ctx.Dbo.Accounts on (customer.Id = account.CustomerId)
        where (customer.EmailAddress = email && account.IsActive = true)
        select account
  } |> Seq.toArray

let deactivateAccount accountId =
  query {
    for account in ctx.Dbo.Accounts do
    where (account.AccountId = accountId)
    select account
  } |> Seq.iter(fun account ->
    account.IsActive <- false
  )

  ctx.SubmitUpdates()

let deActivateCustomer email =
  getActiveAccountsFor email |> Seq.iter (fun account -> deactivateAccount account.AccountId)

let seedData hashedSsn =
  let newClient = ctx.Credit.Clients.Create(SocialSecurityNumberHash = hashedSsn,
                                              IsAuthenticated = false,
                                              L3AuthenticationCounter = 5,
                                              CreateDate = DateTime.Now,
                                              ModifyDate = DateTime.Now,
                                              BrandID = 1)
  ctx.SubmitUpdates()
  newClient.ClientId

let getClientsWithHash hashedSsn =
  query {
    for client in ctx.Credit.Clients do
    where (client.SocialSecurityNumberHash = hashedSsn)
    select client
  } |> Seq.toArray

let deleteVendorSubscriptionFor clientId =
  query {
    for vendorSubscription in ctx.Credit.VendorSubscriptions do
    where (vendorSubscription.ClientId = clientId)
    select vendorSubscription
  } |> Seq.iter(fun v ->
    v.Delete()
  )
  ctx.SubmitUpdates()

let deleteGlobalClient clientId =
  query {
    for globalClient in ctx.Credit.ClientGlobalClient do
    where (globalClient.ClientId = clientId)
    select globalClient
  } |> Seq.iter(fun gc ->
    gc.Delete()
  )
  ctx.SubmitUpdates()

let deleteL3AuthenticationAttempts clientId =
  query {
    for attempt in ctx.Credit.LAuthenticationAttempts do
    where (attempt.ClientId = clientId)
    select attempt
  } |> Seq.iter(fun a ->
    a.Delete()
  )
  ctx.SubmitUpdates()

let deleteClient clientId =
  query {
    for client in ctx.Credit.Clients do
    where (client.ClientId = clientId)
    select client
  } |> Seq.iter(fun c ->
    c.Delete()
  )
  ctx.SubmitUpdates()

let deleteSeedData hashedSsn =
  let clients = getClientsWithHash hashedSsn
  clients |> Seq.iter(fun client -> deleteVendorSubscriptionFor client.ClientId)
  clients |> Seq.iter(fun client -> deleteGlobalClient client.ClientId)
  clients |> Seq.iter(fun client -> deleteL3AuthenticationAttempts client.ClientId)
  clients |> Seq.iter(fun client -> deleteClient client.ClientId)